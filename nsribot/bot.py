import logging

from telegram.ext import Application

from .callout import Poller
from .general import General
from .status import StatusChecker


class Bot:
    def __init__(self, config):
        self.token = config.token
        self.logger = logging.getLogger(__name__)
        self.components = [
            Poller(config.channels.ops, config.channels.admin),
            StatusChecker(),
            General(config.link),
        ]

    def main(self):
        application = Application.builder().token(self.token).build()

        # add handlers for various commands
        for component in self.components:
            for handler in component.get_handlers():
                application.add_handler(handler)

        print("NSRI Polling Bot")
        print("Waiting for Telegram messages... Press Ctrl+C to stop")
        self.logger.info("NSRI Polling bot waiting for telegram messages")

        # Start the Bot
        application.run_polling()
