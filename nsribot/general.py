import logging

from telegram.ext import CommandHandler


class General:
    def __init__(self, link):
        self.link = link

    def get_handlers(self):
        return [
            CommandHandler("start", self.show_link),
        ]

    async def show_link(self, update, context):
        await update.effective_message.reply_text(self.link)
