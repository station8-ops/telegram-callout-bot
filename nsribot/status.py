import logging

from telegram import Chat, ChatMember
from telegram.ext import ChatMemberHandler, CommandHandler, MessageHandler, filters


def extract_status_change(chat_member_update):
    """
    Takes a ChatMemberUpdated instance and extracts whether the 'old_chat_member' was a member
    of the chat and whether the 'new_chat_member' is a member of the chat. Returns None, if
    the status didn't change.
    """
    status_change = chat_member_update.difference().get("status")
    old_is_member, new_is_member = chat_member_update.difference().get(
        "is_member", (None, None)
    )

    if status_change is None:
        return None

    old_status, new_status = status_change
    was_member = old_status in [
        ChatMember.MEMBER,
        ChatMember.CREATOR,
        ChatMember.ADMINISTRATOR,
    ] or (old_status == ChatMember.RESTRICTED and old_is_member is True)
    is_member = new_status in [
        ChatMember.MEMBER,
        ChatMember.CREATOR,
        ChatMember.ADMINISTRATOR,
    ] or (new_status == ChatMember.RESTRICTED and new_is_member is True)

    return was_member, is_member


class StatusChecker:
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def get_handlers(self):
        return [
            MessageHandler(filters.TEXT & ~filters.COMMAND, self.track_chat_messages),
            ChatMemberHandler(
                self.track_chat_membership_changes, ChatMemberHandler.MY_CHAT_MEMBER
            ),
            CommandHandler("show_chats", self.show_chats),
        ]

    # Tracks the chats the bot is in
    def track_chat_membership_changes(self, update, context):
        result = extract_status_change(update.my_chat_member)
        if result is None:
            return
        was_member, is_member = result

        # Let's check who is responsible for the change
        cause_name = update.effective_user.full_name

        # Handle chat types differently:
        chat = update.effective_chat
        if chat.type == Chat.PRIVATE:
            if not was_member and is_member:
                self.logger.info(f"{cause_name} started the bot")
                context.bot_data.setdefault("user_ids", set()).add(chat.id)
            elif was_member and not is_member:
                self.logger.info(f"{cause_name} blocked the bot")
                context.bot_data.setdefault("user_ids", set()).discard(chat.id)
        elif chat.type in [Chat.GROUP, Chat.SUPERGROUP]:
            if not was_member and is_member:
                self.logger.info(
                    f"{cause_name} added the bot to the group {chat.title} ({chat.id})"
                )
                context.bot_data.setdefault("group_ids", set()).add(chat.id)
            elif was_member and not is_member:
                self.logger.info(
                    f"{cause_name} removed the bot from the group {chat.title} ({chat.id})"
                )
                context.bot_data.setdefault("group_ids", set()).discard(chat.id)
        else:
            if not was_member and is_member:
                self.logger.info(
                    f"{cause_name} added the bot to the channel {chat.title} ({chat.id})"
                )
                context.bot_data.setdefault("channel_ids", set()).add(chat.id)
            elif was_member and not is_member:
                self.logger.info(
                    f"{cause_name} removed the bot from the channel {chat.title} ({chat.id})"
                )
                context.bot_data.setdefault("channel_ids", set()).discard(chat.id)

    def track_chat_messages(self, update, context):
        cause_name = update.effective_user.full_name
        chat = update.effective_chat

        if chat.type == Chat.PRIVATE:
            self.logger.info(f"{cause_name} sent a message to the bot")
            context.bot_data.setdefault("user_ids", set()).add(chat.id)
        elif chat.type in [Chat.GROUP, Chat.SUPERGROUP]:
            self.logger.info(
                f"{cause_name} sent a message to the bot in the group {chat.title} ({chat.id})"
            )
            context.bot_data.setdefault("group_ids", set()).add(chat.id)
        else:
            self.logger.info(
                f"{cause_name} sent a message to the bot in the channel {chat.title} ({chat.id})"
            )
            context.bot_data.setdefault("channel_ids", set()).add(chat.id)

    # Returns the chats that the bot is a member of. Useful for finding the chat ID
    # related to specific chats
    async def show_chats(self, update, context):
        self.logger.info(
            f"Received /show_chats command from chat ID {update.effective_chat.title} ({update.effective_chat.id})"
        )
        users = [
            await context.bot.get_chat(uid)
            for uid in context.bot_data.setdefault("user_ids", set())
        ]
        groups = [
            await context.bot.get_chat(gid)
            for gid in context.bot_data.setdefault("group_ids", set())
        ]
        channels = [
            await context.bot.get_chat(cid)
            for cid in context.bot_data.setdefault("channel_ids", set())
        ]

        user_str = ", ".join([f"{c.title} ({c.id})" for c in users])
        group_str = ", ".join([f"{c.title} ({c.id})" for c in groups])
        channel_str = ", ".join([f"{c.title} ({c.id})" for c in channels])

        text = f"@{context.bot.username} is currently"
        if user_str != "":
            text = f"{text} in a conversation with the users [{user_str}]"
            if group_str != "" or channel_str != "":
                text = f"{text}. Moreover it is"
        if group_str != "":
            text = f"{text} a member of the groups [{group_str}]"
        if channel_str != "":
            text = f"{text} an administrator of the channels [{group_str}]"
        text = f"{text}."
        await update.effective_message.reply_text(text)
