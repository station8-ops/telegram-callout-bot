#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from collections import namedtuple

from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler, CommandHandler

Poll = namedtuple("Poll", ["title", "message", "responses"])


class Poller:
    def __init__(self, channel_id, admin_channel_id):
        self.logger = logging.getLogger(__name__)
        # The group chat where polls will be sent
        self.group_chat_id = channel_id
        # The admin group chat where test polls will be sent
        self.group_chat_id_admingrp = admin_channel_id
        # The admin group chat where members can start the poll
        self.authorised_group_chat_id = admin_channel_id

        self.polls = [
            Poll(
                "URGENT STATION CALLOUT",
                "NSRI - HOUT BAY - URGENT CALLOUT Report to the base - Confirm response",
                ["Yes within 15 min", "Yes within 30 min", "No"],
            ),
            Poll(
                "STATION CALLOUT",
                "NSRI - HOUT BAY - CALLOUT respond to the base - Confirm response",
                ["Yes within 30 min", "Yes within 45 min", "No"],
            ),
            Poll(
                "MORE CREW NEEDED",
                "NSRI - HOUT BAY - MORE CREW REQUIRED FOR CALLOUT  - Confirm response",
                ["Yes within 15 min", "Yes within 30 min", "No"],
            ),
            Poll(
                "CALLOUT CANCELLED",
                "NSRI - HOUT BAY - CALL CANCELLED - All resources stand down",
                ["Received", "OK"],  # at least 2 options for a poll, the 'Not Received'
            ),
            Poll(
                "CALLOUT COMPLETED",
                "NSRI - HOUT BAY - CALL COMPLETED - All resources stand down, thank you for your response",
                ["Received", "Well done"],
            ),
            Poll(
                "TEST POLL TO XCOM GROUP",
                "NSRI - HOUT BAY - TEST POLL to XCOM GROUP",
                ["Yes within 15 min", "Yes within 30 min", "No"],
            ),
        ]

        # create the keyboard to select a poll
        self.keyboard = []
        i = 0
        for poll in self.polls:
            self.keyboard.append([InlineKeyboardButton(poll.title, callback_data=i)])
            i += 1

        self.keyboard_markup = InlineKeyboardMarkup(self.keyboard)

    def get_handlers(self):
        return [
            CommandHandler("123", self.start),
            CallbackQueryHandler(self.trigger_poll),
        ]

    # run when the bot reveived a /123 command
    async def start(self, update, context):
        self.logger.info(
            f"Received /123 command from chat ID {update.effective_chat.title} ({update.effective_chat.id})"
        )
        # check if the chat id is from the authorised group.
        if update.effective_chat.id == self.authorised_group_chat_id:
            # send the keyboard to select the poll
            await update.effective_message.reply_text(
                "NSRI Station-8 Quick Call", reply_markup=self.keyboard_markup
            )
            self.logger.debug("Poll options sent to group")
        else:
            self.logger.debug("Chat not authorized to request polls")

    # run when a poll is selected
    async def trigger_poll(self, update, context):
        query = update.callback_query
        await query.answer()
        poll = self.polls[int(query.data)]
        self.logger.info(f"Received poll request {poll.title}")
        if query.data != "5":
            # send the selected poll to the defined group chat_id
            await context.bot.send_poll(
                self.group_chat_id,
                poll.message,
                poll.responses,
                is_anonymous=False,
                allows_multiple_answers=False,
            )
        else:
            await context.bot.send_poll(
                self.group_chat_id_admingrp,
                poll.message,
                poll.responses,
                is_anonymous=False,
                allows_multiple_answers=False,
            )
        # send a message to the requestor to say that the poll has been sent
        await update.effective_message.reply_text(
            "NSRI Station-8 QuickCall Poll sent to the group"
        )
        self.logger.debug("Poll sent to the group")
