import pytest

from nsribot.callout import Poller

from .utils import Chat, Context, Update

POLLS = Poller(1, 2).polls


@pytest.mark.parametrize(
    "callback_data, expected_message",
    [(i, POLLS[i].message) for i in range(len(POLLS))],
)
@pytest.mark.asyncio
async def test_button(callback_data, expected_message):
    update = Update(callback_data)
    context = Context()
    poller = Poller(1, 2)
    await poller.trigger_poll(update, context)
    assert context.bot.message == expected_message


@pytest.mark.asyncio
async def test_poll():
    chat = Chat(2, "Test Admin")
    update = Update(0, chat=chat)
    context = Context()
    poller = Poller(1, 2)
    await poller.start(update, context)
    assert update.effective_message.message == "NSRI Station-8 Quick Call"
    keyboard = update.effective_message.reply_markup.inline_keyboard
    keyboard = [k[0].text for k in keyboard]
    assert keyboard == [p.title for p in POLLS]


@pytest.mark.asyncio
async def test_start_unauthorized():
    chat = Chat(1, "Test Channel")
    update = Update(0, chat=chat)
    context = Context()
    poller = Poller(1, 2)
    await poller.start(update, context)
    # Unauthorized queries should generate no response
    with pytest.raises(AttributeError):
        update.effective_message.message
