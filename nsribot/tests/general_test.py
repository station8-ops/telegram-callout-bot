import pytest

from nsribot.general import General

from .utils import Context, Update


@pytest.mark.asyncio
async def test_start():
    update = Update(0)
    context = Context()
    general = General("https://example.com")
    await general.show_link(update, context)
    assert update.effective_message.message == "https://example.com"
