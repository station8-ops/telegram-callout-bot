class Message:
    async def reply_text(self, message, reply_markup=None):
        self.message = message
        self.reply_markup = reply_markup


class Chat:
    def __init__(self, chat_id, title):
        self.id = chat_id
        self.title = title


class CallbackQuery:
    def __init__(self, data):
        self.data = data

    async def answer(self):
        return True


class Update:
    def __init__(self, callback_data, chat=None):
        self.callback_query = CallbackQuery(callback_data)
        self.effective_chat = chat
        self.effective_message = Message()

    def effective_chat(self):
        return self.effective_chat


class BotInterface:
    async def send_poll(
        self, chat_id, message, responses, is_anonymous, allows_multiple_answers
    ):
        self.chat_id = chat_id
        self.message = message
        self.responses = responses
        self.is_anonymous = is_anonymous
        self.allows_multiple_answers = allows_multiple_answers


class Context:
    bot = BotInterface()
