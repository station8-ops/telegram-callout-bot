import argparse
import logging
import os
import sys

from pyaml_env import BaseConfig, parse_config

from .bot import Bot

LOGGING_FORMAT = "%(asctime)s - %(levelname)s (%(name)s): %(message)s"
DATE_FORMAT = "%Y-%m-%d %H:%M:%S"


def parse_arguments(args):
    """
    The order of precedence is
    1. Commandline flags
    2. Configuration file
    """
    config = {}

    if args.config is not None:
        if not os.path.isfile(args.config):
            raise FileNotFoundError()
        config = parse_config(args.config)

    if config.get("channels") is None:
        config["channels"] = {}
    if args.channel is not None:
        config["channels"]["ops"] = args.channel
    if args.admin is not None:
        config["channels"]["admin"] = args.admin
    if args.token is not None:
        config["token"] = args.token
    if args.link is not None:
        config["link"] = args.link
    return BaseConfig(config)


def run():
    # Get sensitive information from the environment or command line
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c",
        "--channel",
        type=int,
        help="The ID of the channel to which send polls. Defaults to NSRI_BOT_CHANNEL environment variable.",
        default=os.getenv("NSRI_BOT_CHANNEL"),
    )
    parser.add_argument(
        "-a",
        "--admin",
        type=int,
        help="The ID of the admin channel from which users can send polls. Defaults to NSRI_BOT_ADMIN environment variable.",
        default=os.getenv("NSRI_BOT_ADMIN"),
    )
    parser.add_argument(
        "-t",
        "--token",
        help="The token to use for authenticating against Telegram. Defaults to NSRI_BOT_TOKEN environment variable.",
        default=os.getenv("NSRI_BOT_TOKEN"),
    )
    parser.add_argument(
        "-l",
        "--link",
        help="The link to use for start commands when users message the bot. Defaults to NSRI_BOT_LINK environment variable.",
        default=os.getenv("NSRI_BOT_LINK"),
    )
    parser.add_argument(
        "-f",
        "--config",
        help="The configuration file to use",
        default=os.environ.get("NSRI_BOT_CONFIG_FILE"),
    )
    parser.add_argument(
        "-v", "--verbose", help="Show debug information", action="store_true"
    )
    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(
            level=logging.DEBUG, format=LOGGING_FORMAT, datefmt=DATE_FORMAT
        )
    else:
        logging.basicConfig(
            level=logging.INFO, format=LOGGING_FORMAT, datefmt=DATE_FORMAT
        )

    logger = logging.getLogger(__name__)
    try:
        configuration = parse_arguments(args)
        p = Bot(configuration)
        p.main()
    except KeyError:
        logger.error("Error reading config file")
        parser.print_help(file=sys.stderr)
        exit(1)
    except FileNotFoundError:
        logger.error(f"Config file {args.config} does not exist or can not be read")
        parser.print_help(file=sys.stderr)
        exit(1)


if __name__ == "__main__":
    run()
