from python:3

RUN apt-get update -qq && apt-get upgrade -y -qq

ENV NSRI_BOT_CONFIG_FILE=/nsribot.yml

WORKDIR /app

COPY Pipfile Pipfile.lock ./

RUN pip3 install -U pipenv
RUN pipenv install
COPY ./ /app
COPY docker/nsribot.yml /nsribot.yml
CMD pipenv run python3 -m nsribot
